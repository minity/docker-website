#!/bin/sh

USAGE='Usage: docker-run.sh image-suffix hostname [srv-dir [www-dir [mysql-container-name [altrnate-hostnames-comma-delimited]]]]'
IMAGE=${1:?$USAGE}
HOSTNAME=${2:?$USAGE}
SRVDIR=${3:-$PWD}
WWWDIR=${4:-$SRVDIR/www}
MYSQL=${5:-mysql}
NAMES=${6:-www.$HOSTNAME}

docker run -d \
    --name $HOSTNAME \
    -h $HOSTNAME \
    -e VIRTUAL_HOST="$HOSTNAME,$NAMES" \
    -v "$SRVDIR":/srv \
    -v "$WWWDIR":/srv/www \
    --link $MYSQL:mysql \
  local/$IMAGE
