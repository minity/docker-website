#!/bin/sh

IMAGES=${@:-"base nginx-php modx bitrix"}

docker pull ubuntu:latest

for IMAGE in $IMAGES
do
    echo "======== $IMAGE ========"
    docker build -t local/$IMAGE $IMAGE || exit $?
done

docker rmi $(docker images -f "dangling=true" -q)
